const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255) ,cnp  VARCHAR(255), email  VARCHAR(255) , telefon  VARCHAR(255),data_inceput  VARCHAR(10),data_sfarsit  VARCHAR(10), gen VARCHAR(1), varsta INTEGER )"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/bilet", (req, res) => {
  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    gen: req.body.gen,
    varsta: req.body.varsta,
  };
  let error = [];
  //campurile din formular
  if (
    !bilet.nume ||
    !bilet.prenume ||
    !bilet.telefon ||
    !bilet.email ||
    !bilet.cnp ||
    !bilet.data_inceput ||
    !bilet.data_sfarsit ||
    !bilet.gen ||
    !bilet.varsta
  ) {
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  } else {
    if (bilet.nume.length < 2 || bilet.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!bilet.nume.match("^[A-Za-z]+$")) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (bilet.prenume.length < 2 || bilet.prenume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!bilet.prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (bilet.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!bilet.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (
      !bilet.email.includes("@gmail.com") &&
      !bilet.email.includes("@yahoo.com")
    ) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }

    if (
      !bilet.data_inceput.match(
        /^(0?[1-9]|[12][0-9]|3[01])[/-](0?[1-9]|1[012])[/-]\d{4}$/
      )
    ) {
      console.log("Data inceput trebuie sa fie in format dd/mm/yyyy!");
      error.push("Data inceput trebuie sa fie in format dd/mm/yyyy!");
    }

    let data = bilet.data_inceput.split("/");
    if (data[0].length !== 2 || data[1].length !== 2 || data[2].length !== 4) {
      console.log("Data inceput trebuie sa fie in format dd/mm/yyyy!");
      error.push("Data inceput trebuie sa fie in format dd/mm/yyyy!");
    }
    if (
      !bilet.data_sfarsit.match(
        /^(0?[1-9]|[12][0-9]|3[01])[/-](0?[1-9]|1[012])[/-]\d{4}$/
      )
    ) {
      console.log("Data sfarsit trebuie sa fie in format dd/mm/yyyy!");
      error.push("Data sfarsit trebuie sa fie in format dd/mm/yyyy!");
    }
    data = bilet.data_sfarsit.split("/");
    if (data[0].length !== 2 || data[1].length !== 2 || data[2].length !== 4) {
      console.log("Data sfarsit trebuie sa fie in format dd/mm/yyyy!");
      error.push("Data sfarsit trebuie sa fie in format dd/mm/yyyy!");
    }
  }

  if (error.length === 0) {

    const sql = `INSERT INTO abonamente_metrou (nume,
      prenume,
      telefon,
      cnp,
      email,
      data_inceput,
      data_sfarsit,
      gen,
      varsta) VALUES (?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        bilet.nume,
        bilet.prenume,
        bilet.telefon,
        bilet.cnp,
        bilet.email,
        bilet.data_inceput,
        bilet.data_sfarsit,
        bilet.gen,
        bilet.varsta,
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Abonament realizat cu succes!");
        res.status(200).send({
          message: "Abonament realizat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
});
//modifica si din front la index.html
